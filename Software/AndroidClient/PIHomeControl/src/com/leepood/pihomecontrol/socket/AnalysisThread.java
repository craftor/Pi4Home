package com.leepood.pihomecontrol.socket;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * A Thread to find a message form receive datas
 * 
 * @author leepood
 * 
 */
public class AnalysisThread extends Thread {

	private ConcurrentLinkedQueue<byte[]> mData;

	private boolean flag = false;

	public AnalysisThread(ConcurrentLinkedQueue<byte[]> data, boolean flag) {
		setName("AnalysisThread");
		mData = data;
		this.flag = flag;
	}

	@Override
	public void run() {
		// 分析线程，处理
		while (flag) {
			
		}

	}

}
