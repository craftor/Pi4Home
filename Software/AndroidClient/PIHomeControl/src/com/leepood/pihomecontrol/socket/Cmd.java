package com.leepood.pihomecontrol.socket;

/**
 * define all kinds of command
 * 
 * @author leepood
 * 
 */
public enum Cmd {

	/* command of servo control */
	CMD_SERVO(1024),

	/* back command */
	CMD_BACK(1023),

	/* go forward command */
	CMD_FORWARD(1022);

	private int code;

	Cmd(int code) {

	}
}
