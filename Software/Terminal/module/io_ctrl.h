#ifndef _IO_CTRL_H_
#define _IO_CTRL_H_

#define PIN11   0  // Header : pin 11
#define PIN12   1  // Header : pin 12
#define PIN13   2  // Header : pin 13
#define PIN15   3  // Header : pin 15
#define PIN16   4  // Header : pin 16
#define PIN18   5  // Header : pin 18
#define PIN22   6  // Header : pin 22
#define PIN7    7  // Header : pin 7
#define PIN3    8  // Header : pin 3
#define PIN5    9  // Header : pin 5
#define PIN24  10  // Header : pin 24
#define PIN26  11  // Header : pin 26
#define PIN19  12  // Header : pin 19
#define PIN21  13  // Header : pin 21
#define PIN23  14  // Header : pin 23
#define PIN8   15  // Header : pin 8
#define PIN10  16  // Header : pin 10

#endif

