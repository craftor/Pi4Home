
#include "../config.h"
#include "device.h"

int motor_ctrl(unsigned int Direction, unsigned int Length);
int test_device(unsigned int Operation);
int io_ctrl(unsigned int Device, unsigned int Operation);

int init_device() {

	int result = 0;

	result = wiringPiSetup();
	if (result == -1)
	  return result;
	else {
		pinMode(PIN11, OUTPUT);
		pinMode(PIN12, OUTPUT);  /* TXD */
		pinMode(PIN13, OUTPUT);
		pinMode(PIN15, OUTPUT);
		pinMode(PIN16, OUTPUT);
		pinMode(PIN18, OUTPUT);
		pinMode(PIN22, OUTPUT);
		pinMode(PIN7,  OUTPUT);
//		pinMode(PIN3,  OUTPUT);  /* SDA */
//		pinMode(PIN5,  OUTPUT);  /* SCL */
		pinMode(PIN24, OUTPUT);
		pinMode(PIN26, OUTPUT);
		pinMode(PIN19, OUTPUT);
		pinMode(PIN21, OUTPUT);
		pinMode(PIN23, OUTPUT);
		pinMode(PIN8,  OUTPUT);
		pinMode(PIN10,  INPUT);  /* RXD */
	}
	return result;

}

int DeviceOperation(unsigned int Device, unsigned int Command, unsigned int Count, unsigned int *Data) {

	int result;

	switch (Device) {

		case DEVICE_TEST: 
			{
				printf("Test Device : ");
				result = test_device(Command);
				printf(", result=%d.\n", result);
				break;
			}

		case DEVICE_MOTOR: 
			{
				printf("Motor Control : ");
				result = motor_ctrl(Command, Data[0]);
				printf(", result=%d.\n", result);
				break;
			}

		case DEVICE_IO:
			{
				printf("IO Control : ");
				result = io_ctrl(Command, Data[0]);
				printf(", result=%d.\n", result);
				break;
			}

		default:
			{
				result = -1;
				printf("Unknown Device Number.\n");
			}
	}

	return result;
}
