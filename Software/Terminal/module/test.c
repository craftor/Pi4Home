
#include "test.h"
#include "../config.h"

int TestDeviceStatus = TD_OFF;

int test_device(unsigned int Operation) {

	switch (Operation) {
	
		case TD_ON	  : 
			{
				TestDeviceStatus = TD_ON;
				printf("On ");
				break;
			}
		case TD_OFF   : 
			{
				TestDeviceStatus = TD_OFF;
				printf("Off ");
				break;
			}
		case TD_QUERY : 
			{
				printf("Query ");			
				return (TD_QUERY | TestDeviceStatus);
			}
		default:
			return -1;
	
	}
	return 0;
	
}
