#include "../config.h"
#include "io_ctrl.h"

int io_ctrl(unsigned int Device, unsigned int Operation) {

	int result = 0;

	if ((Device <= 16) && (Device >= 0)) {
		switch(Operation) {
			case 0:
				{
					digitalWrite(Device, 0);
					printf("Pin%d Low ", Device);
					break;
				}
			case 1:
				{
					digitalWrite(Device, 1);
					printf("Pin%d High ", Device);
					break;
				}
			default:
				{
					printf("Pin%d -- . ", Device);
					break;
				}
		}
	}else {
		result = -1;
		printf("Pin%d Out Of Range . ", Device);
	}

	return result;
	
}
