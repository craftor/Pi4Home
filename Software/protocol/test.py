
import socket
import os

mydir = "./test_package"

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  
sock.connect(('172.171.40.11', 8001))  

for lists in os.listdir(mydir):
	path = os.path.join(mydir, lists)
	f = open(path, 'rb')
	data1 = f.read(256)
	sock.send(data1)
	data = sock.recv(256)
	f.close()

sock.close()

